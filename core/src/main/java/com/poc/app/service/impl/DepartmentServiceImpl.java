package com.poc.app.service.impl;

import com.poc.app.dto.DepartmentDto;
import com.poc.app.repository.DepartmentRepository;
import com.poc.app.service.DepartmentService;
import com.poc.app.vo.Department;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public DepartmentDto save(DepartmentDto departmentDto) {
        var department = (Department) getDtoToVoConverter().convert(departmentDto);
        departmentDto = (DepartmentDto) getVoToDtoConverter().convert(this.departmentRepository.save(department));
        return departmentDto;
    }

    @Override
    public List<DepartmentDto> findAll() {
        return null;
    }
}
