package com.poc.app.service.impl;


import com.poc.app.dto.CollegeDto;
import com.poc.app.repository.CollegeRepository;
import com.poc.app.service.CollegeService;
import com.poc.app.vo.College;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollegeServiceImpl implements CollegeService {

    private final CollegeRepository collegeRepository;

    public CollegeServiceImpl(CollegeRepository collegeRepository) {
        this.collegeRepository = collegeRepository;
    }

    @Override
    public CollegeDto save(CollegeDto collegeDto) {
        var college = (College) getDtoToVoConverter().convert(collegeDto);
        collegeDto = (CollegeDto) getVoToDtoConverter().convert(this.collegeRepository.save(college));
        return collegeDto;
    }

    @Override
    public List<CollegeDto> findAll() {
        return null;
    }
}
