package com.poc.app.service.impl;

import com.poc.app.dto.StudentDto;
import com.poc.app.repository.StudentRepository;
import com.poc.app.service.StudentService;
import com.poc.app.vo.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public StudentDto save(StudentDto studentDto) {
        var student = (Student) getDtoToVoConverter().convert(studentDto);
        studentDto = (StudentDto) getVoToDtoConverter().convert(this.studentRepository.save(student));
        return studentDto;
    }

    @Override
    public List<StudentDto> findAll() {
        return null;
    }
}
