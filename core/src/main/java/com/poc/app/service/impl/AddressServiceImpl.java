package com.poc.app.service.impl;

import com.poc.app.dto.AddressDto;
import com.poc.app.repository.AddressRepository;
import com.poc.app.service.AddressService;
import com.poc.app.vo.Address;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public List<AddressDto> saveAll(List<AddressDto> addressDtoList) {
        var addressList = addressDtoList.stream().map(addressDto -> (Address) getDtoToVoConverter().convert(addressDto)).collect(Collectors.toList());
        addressDtoList = (List<AddressDto>) getVoToDtoConverter().convert(this.addressRepository.saveAll(addressList));
        return addressDtoList;
    }

    @Override
    public List<AddressDto> findAll() {
        return null;
    }
}
