package com.poc.app.service;

import com.poc.app.converters.Converter;
import com.poc.app.dto.StudentDto;
import com.poc.app.vo.Student;

import java.util.List;
import java.util.function.Supplier;

public interface StudentService {

    Supplier<Student> getStudent = Student::new;
    Supplier<StudentDto> getStudentDto = StudentDto::new;

    default Converter getDtoToVoConverter() {
        return (Converter<StudentDto, Student>) studentDto -> {
            Student student = getStudent.get();
            student.setId(studentDto.getId());
            student.setName(studentDto.getName());
            student.setCreatedTs(studentDto.getCreatedTs());
            return student;
        };
    }

    default Converter getVoToDtoConverter() {
        return (Converter<Student, StudentDto>) student -> {
            StudentDto studentDto = getStudentDto.get();
            studentDto.setId(student.getId());
            studentDto.setName(student.getName());
            studentDto.setCreatedTs(student.getCreatedTs());
            return studentDto;
        };
    }

    StudentDto save(StudentDto student);

    List<StudentDto> findAll();
}
