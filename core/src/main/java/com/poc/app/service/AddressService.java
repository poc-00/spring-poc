package com.poc.app.service;

import com.poc.app.converters.Converter;
import com.poc.app.dto.AddressDto;
import com.poc.app.vo.Address;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public interface AddressService {
    Supplier<Address> getAddress = Address::new;
    Supplier<AddressDto> getAddressDto = AddressDto::new;

    default Converter getDtoToVoConverter() {
        return (Converter<AddressDto, Address>) addressDto -> {
            Address address = getAddress.get();
            address.setId(addressDto.getId());
            address.setAddress(addressDto.getAddress());
            address.setStudentId(addressDto.getStudentId());
            address.setCreatedTs(addressDto.getCreatedTs());
            return address;
        };
    }

    default Converter getVoToDtoConverter() {
        return (Converter<List<Address>, List<AddressDto>>) addressList -> addressList.stream().map(address -> {
            AddressDto addressDto = getAddressDto.get();
            addressDto.setId(address.getId());
            addressDto.setAddress(address.getAddress());
            addressDto.setStudentId(address.getStudentId());
            addressDto.setCreatedTs(address.getCreatedTs());
            return addressDto;
        }).collect(Collectors.toList());
    }

    List<AddressDto> saveAll(List<AddressDto> addressDtoList);

    List<AddressDto> findAll();
}
