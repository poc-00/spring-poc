package com.poc.app.service;

import com.poc.app.converters.Converter;
import com.poc.app.dto.DepartmentDto;
import com.poc.app.vo.Department;

import java.util.List;
import java.util.function.Supplier;

public interface DepartmentService {
    Supplier<Department> getDepartment = Department::new;
    Supplier<DepartmentDto> getDepartmentDto = DepartmentDto::new;

    default Converter getDtoToVoConverter() {
        return (Converter<DepartmentDto, Department>) departmentDto -> {
            Department department = getDepartment.get();
            department.setId(departmentDto.getId());
            department.setName(departmentDto.getName());
            department.setStudentId(departmentDto.getStudentId());
            department.setCreatedTs(departmentDto.getCreatedTs());
            return department;
        };
    }

    default Converter getVoToDtoConverter() {
        return (Converter<Department, DepartmentDto>) department -> {
            DepartmentDto departmentDto = getDepartmentDto.get();
            departmentDto.setId(department.getId());
            departmentDto.setName(department.getName());
            departmentDto.setStudentId(department.getStudentId());
            departmentDto.setCreatedTs(department.getCreatedTs());
            return departmentDto;
        };
    }

    DepartmentDto save(DepartmentDto departmentDto) throws Exception;

    List<DepartmentDto> findAll();
}
