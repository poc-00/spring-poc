package com.poc.app.service;

import com.poc.app.converters.Converter;
import com.poc.app.dto.CollegeDto;
import com.poc.app.vo.College;

import java.util.List;
import java.util.function.Supplier;

public interface CollegeService {
    Supplier<College> getCollege = College::new;
    Supplier<CollegeDto> getCollegeDto = CollegeDto::new;

    default Converter getDtoToVoConverter() {
        return (Converter<CollegeDto, College>) collegeDto -> {
            College college = getCollege.get();
            college.setId(collegeDto.getId());
            college.setName(collegeDto.getName());
            college.setStudentId(collegeDto.getStudentId());
            college.setDepartmentId(collegeDto.getDepartmentId());
            college.setCreatedTs(collegeDto.getCreatedTs());
            return college;
        };
    }

    default Converter getVoToDtoConverter() {
        return (Converter<College, CollegeDto>) college -> {
            CollegeDto collegeDto = getCollegeDto.get();
            collegeDto.setId(college.getId());
            collegeDto.setName(college.getName());
            collegeDto.setStudentId(college.getStudentId());
            collegeDto.setDepartmentId(college.getDepartmentId());
            collegeDto.setCreatedTs(college.getCreatedTs());
            return collegeDto;
        };
    }

    CollegeDto save(CollegeDto collegeDto);

    List<CollegeDto> findAll();
}
