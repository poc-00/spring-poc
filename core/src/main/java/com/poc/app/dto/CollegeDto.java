package com.poc.app.dto;

import java.util.Date;

public class CollegeDto {
    private int id;
    private int studentId;
    private int departmentId;
    private String name;
    private Date createdTs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(Date createdTs) {
        this.createdTs = createdTs;
    }

    @Override
    public String toString() {
        return "College{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", departmentId=" + departmentId +
                ", name='" + name + '\'' +
                '}';
    }
}
