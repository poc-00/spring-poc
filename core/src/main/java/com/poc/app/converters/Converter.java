package com.poc.app.converters;

@FunctionalInterface
public interface Converter<T,V> {
    V convert(T type);
}
