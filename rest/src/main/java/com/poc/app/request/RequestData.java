package com.poc.app.request;

import com.poc.app.dto.AddressDto;
import com.poc.app.dto.CollegeDto;
import com.poc.app.dto.DepartmentDto;
import com.poc.app.dto.StudentDto;

import java.util.List;

public class RequestData {
    private StudentDto studentDto;
    private DepartmentDto departmentDto;
    private CollegeDto collegeDto;
    private List<AddressDto> addressDtoList;

    public StudentDto getStudentDto() {
        return studentDto;
    }

    public void setStudentDto(StudentDto studentDto) {
        this.studentDto = studentDto;
    }

    public DepartmentDto getDepartmentDto() {
        return departmentDto;
    }

    public void setDepartmentDto(DepartmentDto departmentDto) {
        this.departmentDto = departmentDto;
    }

    public CollegeDto getCollegeDto() {
        return collegeDto;
    }

    public void setCollegeDto(CollegeDto collegeDto) {
        this.collegeDto = collegeDto;
    }

    public List<AddressDto> getAddressDtoList() {
        return addressDtoList;
    }

    public void setAddressDtoList(List<AddressDto> addressDtoList) {
        this.addressDtoList = addressDtoList;
    }

    @Override
    public String toString() {
        return "RequestData{" +
                "studentDto=" + studentDto +
                ", departmentDto=" + departmentDto +
                ", collegeDto=" + collegeDto +
                ", addressDtoList=" + addressDtoList +
                '}';
    }
}
