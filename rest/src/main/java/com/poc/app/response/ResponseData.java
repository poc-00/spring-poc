package com.poc.app.response;

import com.poc.app.dto.AddressDto;
import com.poc.app.dto.CollegeDto;
import com.poc.app.dto.DepartmentDto;
import com.poc.app.dto.StudentDto;

import java.util.List;

public class ResponseData {
    private StudentDto studentDto;
    private DepartmentDto departmentDto;
    private CollegeDto collegeDto;
    private List<AddressDto> addressDtoList;
    private String status;

    public StudentDto getStudentDto() {
        return studentDto;
    }

    public void setStudentDto(StudentDto studentDto) {
        this.studentDto = studentDto;
    }

    public DepartmentDto getDepartmentDto() {
        return departmentDto;
    }

    public void setDepartmentDto(DepartmentDto departmentDto) {
        this.departmentDto = departmentDto;
    }

    public CollegeDto getCollegeDto() {
        return collegeDto;
    }

    public void setCollegeDto(CollegeDto collegeDto) {
        this.collegeDto = collegeDto;
    }

    public List<AddressDto> getAddressDtoList() {
        return addressDtoList;
    }

    public void setAddressDtoList(List<AddressDto> addressDtoList) {
        this.addressDtoList = addressDtoList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
