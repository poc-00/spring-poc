package com.poc.app.service;

import com.poc.app.request.RequestData;
import com.poc.app.response.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class StudentResponseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentResponseService.class);

    private final Supplier<ResponseData> responseData = ResponseData::new;

    private final StudentService studentService;

    private final DepartmentService departmentService;

    private final AddressService addressService;

    private final CollegeService collegeService;

    public StudentResponseService(StudentService studentService, DepartmentService departmentService, AddressService addressService, CollegeService collegeService) {
        this.studentService = studentService;
        this.departmentService = departmentService;
        this.addressService = addressService;
        this.collegeService = collegeService;
    }

    @Transactional(rollbackFor = Exception.class)
    public ResponseData createStudent(RequestData requestData) throws Exception {
        LOGGER.info("In Create Student {}", requestData);
        var responseData = this.responseData.get();
        try {
            var studentDto = this.studentService.save(requestData.getStudentDto());
            requestData.getDepartmentDto().setStudentId(studentDto.getId());
            var departmentDto = this.departmentService.save(requestData.getDepartmentDto());
            requestData.getCollegeDto().setStudentId(studentDto.getId());
            requestData.getCollegeDto().setDepartmentId(departmentDto.getId());
            var collegeDto = this.collegeService.save(requestData.getCollegeDto());
            var addressList = requestData.getAddressDtoList().stream().map(addressDto -> {
                addressDto.setStudentId(studentDto.getId());
                return addressDto;
            }).collect(Collectors.toList());
            var addressDtoList = this.addressService.saveAll(addressList);

            responseData.setStudentDto(studentDto);
            responseData.setDepartmentDto(departmentDto);
            responseData.setCollegeDto(collegeDto);
            responseData.setAddressDtoList(addressDtoList);
            responseData.setStatus("Success");
        } catch (Exception e) {
            e.printStackTrace();
            responseData.setStatus("Failed");
            throw e;
        }
        return responseData;
    }
}
