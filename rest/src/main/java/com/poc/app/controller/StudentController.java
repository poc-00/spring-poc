package com.poc.app.controller;

import com.poc.app.request.RequestData;
import com.poc.app.response.ResponseData;
import com.poc.app.service.StudentResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("student")
public class StudentController {

    @Autowired
    private StudentResponseService studentResponseService;

    @PostMapping("create-student")
    public ResponseData createStudent(@RequestBody RequestData requestData) throws Exception{
        ResponseData responseData = studentResponseService.createStudent(requestData);
        return responseData;
    }
}
