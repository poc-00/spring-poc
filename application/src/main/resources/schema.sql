create table STUDENTS
(
	id int auto_increment,
	name varchar2(50) not null,
	created_ts TIMESTAMP,
	constraint STUDENTS_PK
		primary key (id)
);

create table DEPARTMENTS
(
	id int auto_increment,
	student_id int,
	name varchar2(50) not null,
	created_ts TIMESTAMP,
	constraint DEPARTMENTS_PK
		primary key (id)
);

create table COLLEGES
(
	id int auto_increment,
	name varchar2(50) not null,
	student_id int,
	department_id int,
	created_ts TIMESTAMP,
	constraint COLLEGES_PK
		primary key (id)
);

create table ADDRESS
(
	id int auto_increment,
	address varchar2(50) not null,
	student_id int,
	created_ts TIMESTAMP,
	constraint ADDRESS_PK
		primary key (id)
);
