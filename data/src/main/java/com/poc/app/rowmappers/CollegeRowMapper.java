package com.poc.app.rowmappers;

import com.poc.app.vo.College;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CollegeRowMapper implements RowMapper<College> {
    @Override
    public College mapRow(ResultSet resultSet, int i) throws SQLException {
        var college = new College();
        college.setId(resultSet.getInt("ID"));
        college.setStudentId(resultSet.getInt("STUDENT_ID"));
        college.setDepartmentId(resultSet.getInt("DEPARTMENT_ID"));
        college.setName(resultSet.getString("NAME"));
        college.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
        return college;
    }
}
