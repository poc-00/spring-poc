package com.poc.app.rowmappers;

import com.poc.app.vo.Address;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressRowMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        var address = new Address();
        address.setId(resultSet.getInt("ID"));
        address.setStudentId(resultSet.getInt("STUDENT_ID"));
        address.setAddress(resultSet.getString("ADDRESS"));
        address.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
        return address;
    }
}
