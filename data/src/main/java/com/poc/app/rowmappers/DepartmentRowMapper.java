package com.poc.app.rowmappers;

import com.poc.app.vo.Department;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartmentRowMapper implements RowMapper<Department> {
    @Override
    public Department mapRow(ResultSet resultSet, int i) throws SQLException {
        var department = new Department();
        department.setId(resultSet.getInt("ID"));
        department.setStudentId(resultSet.getInt("STUDENT_ID"));
        department.setName(resultSet.getString("NAME"));
        department.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
        return department;
    }
}
