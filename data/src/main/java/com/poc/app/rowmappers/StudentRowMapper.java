package com.poc.app.rowmappers;

import com.poc.app.vo.Student;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentRowMapper implements RowMapper<Student> {
    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {
        Student student = new Student();
        student.setId(resultSet.getInt("ID"));
        student.setName(resultSet.getString("NAME"));
        student.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
        return student;
    }
}
