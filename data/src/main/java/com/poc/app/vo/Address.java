package com.poc.app.vo;

import java.util.Date;

public class Address {
    private int id;
    private int studentId;
    private String address;
    private Date createdTs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(Date createdTs) {
        this.createdTs = createdTs;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", address='" + address + '\'' +
                '}';
    }
}
