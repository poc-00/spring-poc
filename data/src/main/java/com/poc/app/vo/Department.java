package com.poc.app.vo;

import java.util.Date;

public class Department {
    private int id;
    private int studentId;
    private String name;
    private Date createdTs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedTs() {
        return createdTs;
    }

    public void setCreatedTs(Date createdTs) {
        this.createdTs = createdTs;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", name='" + name + '\'' +
                '}';
    }
}
