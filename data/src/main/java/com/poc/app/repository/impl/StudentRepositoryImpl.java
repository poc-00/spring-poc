package com.poc.app.repository.impl;

import com.poc.app.repository.StudentRepository;
import com.poc.app.rowmappers.StudentRowMapper;
import com.poc.app.vo.Student;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public StudentRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<Student> findAll() {
        var query = "select id,name,CREATED_TS from STUDENTS";
        List<Student> students = this.namedParameterJdbcTemplate.query(query, new StudentRowMapper());
        return students;
    }

    @Override
    public Student save(Student student) {
        var holder = new GeneratedKeyHolder();
        var date = new Date();
        var query = "INSERT INTO STUDENTS(NAME,CREATED_TS) VALUES(:NAME, :CREATED_TS)";
        var map = Map.of("NAME", student.getName(), "CREATED_TS", date);
        var sqlParameterSource = new MapSqlParameterSource(map);
        var count = this.namedParameterJdbcTemplate.update(query, sqlParameterSource, holder);
        student.setId(Objects.requireNonNull(holder.getKey()).intValue());
        student.setCreatedTs(date);
        return student;
    }
}
