package com.poc.app.repository.impl;

import com.poc.app.repository.CollegeRepository;
import com.poc.app.vo.College;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class CollegeRepositoryImpl implements CollegeRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public CollegeRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<College> findAll() {
        var query = "select id,name,STUDENT_ID,DEPARTMENT_ID,CREATED_TS from COLLEGES";
        List<College> colleges = this.namedParameterJdbcTemplate.query(query, (resultSet, i) -> {
            var college = new College();
            college.setId(resultSet.getInt("ID"));
            college.setStudentId(resultSet.getInt("STUDENT_ID"));
            college.setDepartmentId(resultSet.getInt("DEPARTMENT_ID"));
            college.setName(resultSet.getString("NAME"));
            college.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
            return college;
        });
        return colleges;
    }

    @Override
    public College save(College college) {
        var holder = new GeneratedKeyHolder();
        var date = new Date();
        var query = "INSERT INTO COLLEGES(NAME,STUDENT_ID,DEPARTMENT_ID,CREATED_TS) VALUES(:NAME,:STUDENT_ID,:DEPARTMENT_ID, :CREATED_TS)";
        var map = Map.of("NAME", college.getName(),
                "STUDENT_ID", college.getStudentId(),
                "DEPARTMENT_ID", college.getDepartmentId(),
        "CREATED_TS", date);
        var sqlParameterSource = new MapSqlParameterSource(map);
        var count = this.namedParameterJdbcTemplate.update(query, sqlParameterSource, holder);
        college.setId(Objects.requireNonNull(holder.getKey()).intValue());
        college.setCreatedTs(date);
        return college;
    }
}
