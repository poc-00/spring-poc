package com.poc.app.repository;

import com.poc.app.vo.Student;

public interface StudentRepository extends Repository<Student> {
}
