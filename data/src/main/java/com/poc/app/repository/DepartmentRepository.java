package com.poc.app.repository;

import com.poc.app.vo.Department;

public interface DepartmentRepository extends Repository<Department> {
}
