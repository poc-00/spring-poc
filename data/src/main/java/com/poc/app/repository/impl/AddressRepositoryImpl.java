package com.poc.app.repository.impl;

import com.poc.app.repository.AddressRepository;
import com.poc.app.vo.Address;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class AddressRepositoryImpl implements AddressRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public AddressRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<Address> saveAll(List<Address> addresses) {
        var query = "INSERT INTO ADDRESS(STUDENT_ID,ADDRESS,CREATED_TS) VALUES(:STUDENT_ID,:ADDRESS, CURRENT_TIMESTAMP)";
        Map[] map = new HashMap[addresses.size()];
        for (int i = 0; i < addresses.size(); i++) {
            int finalI = i;
            map[i] = new HashMap() {{
                put("STUDENT_ID", addresses.get(finalI).getStudentId());
                put("ADDRESS", addresses.get(finalI).getAddress());
            }};
        }

        var count = this.namedParameterJdbcTemplate.batchUpdate(query, map);

        Optional<List<Address>> optional = Optional.empty();

        if (count.length > 0) {
            optional = this.findByStudentId(addresses.get(0).getStudentId());
        }

        return optional.isPresent() ? optional.get() : Collections.emptyList();
    }

    @Override
    public List<Address> findAll() {
        var query = "select id,STUDENT_ID,ADDRESS,CREATED_TS from ADDRESS";
        List<Address> addresses = this.namedParameterJdbcTemplate.query(query, (resultSet, i) -> {
            var address = new Address();
            address.setId(resultSet.getInt("ID"));
            address.setStudentId(resultSet.getInt("STUDENT_ID"));
            address.setAddress(resultSet.getString("ADDRESS"));
            address.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
            return address;
        });
        return addresses;
    }

    public Optional<List<Address>> findByStudentId(int studentId) {
        var query = "select id,STUDENT_ID,ADDRESS,CREATED_TS from ADDRESS where STUDENT_ID=:STUDENT_ID";
        Map map = Map.of("STUDENT_ID", studentId);
        List<Address> addresses = this.namedParameterJdbcTemplate.query(query, map, (resultSet, i) -> {
            var address = new Address();
            address.setId(resultSet.getInt("ID"));
            address.setStudentId(resultSet.getInt("STUDENT_ID"));
            address.setAddress(resultSet.getString("ADDRESS"));
            address.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
            return address;
        });
        return Optional.of(addresses);
    }

}
