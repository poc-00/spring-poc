package com.poc.app.repository;

import com.poc.app.vo.College;

public interface CollegeRepository extends Repository<College>{
}
