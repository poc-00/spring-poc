package com.poc.app.repository.impl;

import com.poc.app.repository.DepartmentRepository;
import com.poc.app.vo.Department;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class DepartmentRepositoryImpl implements DepartmentRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public DepartmentRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<Department> findAll() {
        var query = "select id,name,STUDENT_ID,CREATED_TS from DEPARTMENTS";
        List<Department> departments = this.namedParameterJdbcTemplate.query(query, (resultSet, i) -> {
            var department = new Department();
            department.setId(resultSet.getInt("ID"));
            department.setStudentId(resultSet.getInt("STUDENT_ID"));
            department.setName(resultSet.getString("NAME"));
            department.setCreatedTs(resultSet.getTimestamp("CREATED_TS"));
            return department;
        });
        return departments;
    }

    @Override
    public Department save(Department department) {
        var holder = new GeneratedKeyHolder();
        var date = new Date();
        var query = "INSERT INTO DEPARTMENTS(NAME,STUDENT_ID,CREATED_TS) VALUES(:NAME,:STUDENT_ID, :CREATED_TS)";
        var map = Map.of("NAME", department.getName(), "STUDENT_ID", department.getStudentId(), "CREATED_TS", date);
        var sqlParameterSource = new MapSqlParameterSource(map);
        var count = this.namedParameterJdbcTemplate.update(query, sqlParameterSource, holder);
        department.setId(Objects.requireNonNull(holder.getKey()).intValue());
        department.setCreatedTs(date);
        return department;
    }
}
