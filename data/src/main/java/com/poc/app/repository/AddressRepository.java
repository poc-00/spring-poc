package com.poc.app.repository;

import com.poc.app.vo.Address;

import java.util.List;
import java.util.Optional;

public interface AddressRepository extends Repository<Address> {
    default Address save(Address entity) {
        return null;
    }
    List<Address> saveAll(List<Address> addresses);
    Optional<List<Address>> findByStudentId(int studentId);
}
